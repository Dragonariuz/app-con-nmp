FROM node
MAINTAINER Dante Rodriguez
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
