const { Sequelize } = require("sequelize");

module.exports = (sequelize, type) =>{
    const Copy = sequelize.define('copies',{
        id: {type: type.INTEGER, primaryKey:true, autoIncrement:true},
        number: type.INTEGER,
        format: {type: Sequelize.ENUM("VHS","DVD","Blueray")},
        status: {type: Sequelize.ENUM("AVAILABLE","RENTED","LOST")}
    })
    return Copy;
};
