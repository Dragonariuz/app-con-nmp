const express = require('express'); //Exportar express
const { Director } = require('../db');

function list(req, res, next) {
    Director.findAll({include:['movies']})
                     .then(objects => res.json(objects))
                     .catch(err => res.send(err));    
}

function index(req, res, next) {
    const id =req.params.id;
    Director.findByPk(id)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    

    let director = new Object({
        name: name,
        lastName: lastName
    });

    Director.create(director)
        .then(obj => res.json(obj))
        .catch(error => res.send(error));
}

function replace(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then((object)=>{
            const name = req.body.name ? req.body.name: "";
            const lastName = req.body.lastName ? req.body.lastName: false;
            object.update({name:name, lastName:lastName})
                .then(director => res.json(director))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}

function update(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then((object)=>{
            const name = req.body.name ? req.body.name: object.name;
            const lastName = req.body.lastName ? req.body.lastName: object.lastName;
            object.update({name:name, lastName:lastName})
                .then(director => res.json(director))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Director.destroy({where:{id:id}})
        .then(obj  => res.json(obj))
        .catch(res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};

/*
dominio-> User => /users
method  url             action
Get     /users/ ->      list
Get     /userss/{id} -> index

Post    /users/ ->      Create (parameters in body)
Put     /users/{id} ->  replace (parameters in body)
Patch   /users/{id} ->  update
Delete  /users/{id} ->  destroy (parameters in body)
*/

/*
    req.params -> 
    req.body -> parametros de la peticion
*/