const express = require('express'); //Exportar express
const { Member } = require('../db');

function list(req, res, next) {
    Member.findAll({include:['bookings']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));    
}

function index(req, res, next) {
    const id =req.params.id;
    Member.findByPk(id)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const address = req.body.address;
    const phone = req.body.phone; 
    const status = req.body.status;

    let member = new Object({
        name: name,
        lastName: lastName,
        address: address,
        phone: phone,
        status: status
    });

    Member.create(member)
        .then(obj => res.json(obj))
        .catch(error => res.send(error));
}


function replace(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then((object)=>{
            const name = req.body.name ? req.body.name: "";
            const lastName = req.body.lastName ? req.body.lastName: "";
            const address = req.body.address ? req.body.address: "";
            const phone = req.body.phone ? req.body.phone: "";
            const status = req.body.status ? req.body.status: false;
            object.update({name:name, lastName: lastName, address: address, phone: phone, status:status})
                .then(member => res.json(member))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}


function update(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then((object)=>{
        const name = req.body.name ? req.body.name: object.name;
        const lastName = req.body.lastName ? req.body.lastName: object.lastName;
        const address = req.body.address ? req.body.address: object.address;
        const phone = req.body.phone ? req.body.phone: object.phone;
        const status = req.body.status ? req.body.status: false;
        object.update({name:name, lastName: lastName, address: address, phone: phone, status:status})
                .then(member => res.json(member))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.destroy({where:{id:id}})
        .then(obj  => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};

/*
dominio-> User => /users
method  url             action
Get     /users/ ->      list
Get     /userss/{id} -> index

Post    /users/ ->      Create (parameters in body)
Put     /users/{id} ->  replace (parameters in body)
Patch   /users/{id} ->  update
Delete  /users/{id} ->  destroy (parameters in body)
*/

/*
    req.params -> 
    req.body -> parametros de la peticion
*/