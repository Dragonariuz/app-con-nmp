const express = require('express'); //Exportar express
const { Genre } = require('../db');

function list(req, res, next) {
    Genre.findAll({include:['movies']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));    
}

function index(req, res, next) {
    const id =req.params.id;
    Genre.findByPk(id)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

function create(req, res, next) {
    const description = req.body.description;
    const status = req.body.status;

    let genre = new Object({
        description: description,
        status: status
    });

    Genre.create(genre)
        .then(obj => res.json(obj))
        .catch(error => res.send(error));
}


function replace(req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id).then((object)=>{
            const description = req.body.description ? req.body.description: "";
            const status = req.body.status ? req.body.status: false;
            object.update({description:description, status:status})
                .then(genre => res.json(genre))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}


function update(req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id).then((object)=>{
            const description = req.body.description ? req.body.description: object.description;
            const status = req.body.status ? req.body.status: object.status;
            object.update({description:description, status:status})
                .then(genre => res.json(genre))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Genre.destroy({where:{id:id}})
        .then(obj  => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};

/*
dominio-> User => /users
method  url             action
Get     /users/ ->      list
Get     /userss/{id} -> index

Post    /users/ ->      Create (parameters in body)
Put     /users/{id} ->  replace (parameters in body)
Patch   /users/{id} ->  update
Delete  /users/{id} ->  destroy (parameters in body)
*/

/*
    req.params -> 
    req.body -> parametros de la peticion
*/