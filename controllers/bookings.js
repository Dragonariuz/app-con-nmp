const express = require('express'); //Exportar express
const { Booking } = require('../db');

function list(req, res, next) {
    Booking.findAll({include:['member, copy']})
            .then(objects => res.json(objects))
            .catch(err => res.send(err));    
}

function index(req, res, next) {
    const id =req.params.id;
    Booking.findByPk(id)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

function create(req, res, next) {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copyId = req.body.copyId;
  
    let booking = new Object({
      date:date,
      memberId:memberId,
      copyId:copyId
    });
  
    Booking.create(booking)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}


function replace(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then((object)=>{
            const date = req.body.date ? req.body.date: "";
            const memberId = req.body.memberId ? req.body.memberId: "";
            const copyId = req.body.copyId ? req.body.copyId: "";
            object.update({date:date, memberId:memberId, copyId:copyId})
                .then(booking => res.json(booking))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}


function update(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then((object)=>{
            const date = req.body.date ? req.body.date: object.date;
            const memberId = req.body.memberId ? req.body.memberId: object.memberId;
            const copyId = req.body.copyId ? req.body.copyId: object.copyId;
            object.update({date:date, memberId:memberId, copyId:copyId})
                .then(booking => res.json(booking))
                .catch(error => res.send(error));
    }).catch(error => res.send(error));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.destroy({where:{id:id}})
        .then(obj  => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {list, index, create, replace, update, destroy};

/*
dominio-> User => /users
method  url             action
Get     /users/ ->      list
Get     /userss/{id} -> index

Post    /users/ ->      Create (parameters in body)
Put     /users/{id} ->  replace (parameters in body)
Patch   /users/{id} ->  update
Delete  /users/{id} ->  destroy (parameters in body)
*/

/*
    req.params -> 
    req.body -> parametros de la peticion
*/