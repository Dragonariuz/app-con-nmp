var express = require('express');
var router = express.Router();

const controller = require('../controllers/directors')

/* GET users listing. */
router.get('/', controller.list);

router.get('/:id', controller.index);

router.post('/', controller.create);

router.put('/:id', controller.replace);

router.patch('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;


/* Paso de parametros
Explicito       implicito                           explicito
url         {/_/ = hhtp://myapp.com/users/1/2  o http://myapp.com/users?id=1&type=2}
*/