var express = require('express');
var router = express.Router();

/* GET home page. */ //Se declara el home (dominio) de la app y el de la subdivision (index)
router.get('/', function(req, res, next) { 
  res.render('index', { title: 'Express' }); 
});

module.exports = router;
