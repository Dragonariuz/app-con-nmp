const Sequelize = require('sequelize');

const genreModel = require('./models/genre')
const movieModel = require('./models/movie')
const directorModel = require('./models/director')
const copyModel = require('./models/copy')
const bookingModel = require('./models/booking')
const memberModel = require('./models/member');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');

// 1) Nombre de la base de datos
// 2) Usuario de la base de datos
// 3) Contraseña de la base de datos
// 4) Objeto de configuracion (ORM)

const sequelize = new Sequelize('video_club', 'root',
 'secret', {
    host:'127.0.0.1',
    dialect:'mysql'
});

const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);

// Un Genero puede tener muchas peliculas
Genre.hasMany(Movie,{as:'movies'});
// Una pelicula puede tener un Genero
Movie.belongsTo(Genre,{as:'genre'});

// Un Director puede tener muchas peliculas
Director.hasMany(Movie,{as:'movies'});
// Una pelicula puede tener un Director
Movie.belongsTo(Director,{as:'director'});

//Una pelicula puede tener muchas copias
Movie.hasMany(Copy,{as:'copies'});
//Una copia solo es de una pelicula
Copy.belongsTo(Movie,{as:'movie'});

//Una copia puede pertenecer a muchas rentas
Copy.hasMany(Booking,{as:'bookings'})
//Una renta solo es de una copia
Booking.belongsTo(Copy,{as:'copy'})

//Un miembro puede tener muchas rentas
Member.hasMany(Booking,{as:'bookings'});
//Las rentas son de un solo miembro
Booking.belongsTo(Member,{as:'member'});

//Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie,{foreignKey: 'movieId'});
//En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey: 'actorId'});

//Cuantos actores tiene una pelicula
Movie.belongsToMany(Actor, {
    foreignKey : 'actorId',
    as: 'actors',
    through: 'moviesActors'
});

//En cuantas pelis ha actuado un actor
Actor.belongsToMany(Movie, {
    foreignKey : 'movieId',
    as: 'movies',
    through: 'moviesActors'
});

sequelize.sync({
    force:true
}).then(()=>{
    console.log("Base de datos actualizada.");
});

module.exports = { Genre, Movie, Director, Copy, Booking, Member, Actor };
